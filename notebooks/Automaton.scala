package automaton

abstract class Automaton[State,Letter](
  Q: Set[State],
  Sigma: Set[Letter],
  q0: State,
  F: Set[State]
) {
  def _deltaKeys: Seq[(State,Option[Letter])]
  def _delta(q: State, x: Option[Letter]): Set[State]
  def eClosure(qs: Set[State]): Set[State] = {
    val qs1 = for (q <- qs; q1 <- _delta(q,None)) yield q1
    val qs2 = qs union qs1
    if (qs == qs2) qs
    else eClosure(qs2)
  }
  def transition1(qs: Set[State], a: Letter): Set[State] = {
    val qs1 = eClosure(qs)
    val qs2 = for (q <- qs1; q1 <- _delta(q,Some(a))) yield q1
    eClosure(qs2)
  }
  def transition(qs: Set[State], w: Seq[Letter]): Set[State] =
    if (w.isEmpty) qs
    else transition(transition1(qs, w.head), w.tail)
  def accept(w: Seq[Letter]): Boolean =
    transition(Set(q0), w).exists(q => F.contains(q))
  def reachableStates(qs: Set[State]): Set[State] = {
    val qs1 = for (q <- qs; (q1,x) <- _deltaKeys; if q == q1; q2 <- _delta(q,x)) yield q2
    val qs2 = qs union qs1
    if (qs == qs2) qs
    else reachableStates(qs2)
  }
  def toDFA(simplify: Boolean = true): DFA[Set[State],Letter] = {
    val QD = for (qs <- Q.subsets.toSet) yield eClosure(qs)
    val SigmaD = Sigma
    val deltaD: Map[(Set[State],Letter),Set[State]] =
      (for (R <- QD; a <- SigmaD) yield {
        val qs = for (r <- R; q <- _delta(r, Some(a))) yield q
        (R,a) -> eClosure(qs)
      }).toMap
    val qD = eClosure(Set(q0))
    val FD = for (R <- QD; if ! R.intersect(F).isEmpty) yield R
    val dfa = DFA(QD, SigmaD, deltaD, qD, FD)
    if (simplify) dfa.simplify
    else dfa
  }
  def toGraphviz: String = {
    val str = Q.map(q => (q -> q.toString.replaceAll("Set",""))).toMap
    val sb = new StringBuilder
    sb.append("digraph automaton {\n")
    sb.append("  rankdir=LR;\n")
    sb.append("  node [shape=circle];\n")
    sb.append("  start [shape=plaintext,label=\" \"];\n")
    for (q <- F)
      sb.append(s"""  "${str(q)}" [shape=doublecircle];""" + "\n")
    sb.append(s"""  start -> "${str(q0)}";""" + "\n")
    for ((q,None) <- _deltaKeys; q1 <- _delta(q, None))
      sb.append(s"""  "${str(q)}" -> "${str(q1)}" [label="ε"];""" + "\n")
    for ((q,Some(a)) <- _deltaKeys; q1 <- _delta(q, Some(a)))
      sb.append(s"""  "${str(q)}" -> "${str(q1)}" [label="$a"];""" + "\n")
    sb.append("}")
    sb.toString
  }
  def toPNG(fileName: String = "output.png"): Unit = {
    import scala.sys.process._
    val str = toGraphviz
    val file = new java.io.File(fileName)
    val cmd = "dot" :: "-Tpng" :: "-s300" :: Nil
    val is = new java.io.ByteArrayInputStream(str.getBytes("UTF-8"))
    (cmd #< is #> file).!
  }
}

case class DFA[State,Letter](
  Q: Set[State],
  Sigma: Set[Letter],
  delta: Map[(State,Letter),State],
  q0: State,
  F: Set[State]
) extends Automaton(Q, Sigma, q0, F) {
  def _deltaKeys = for ((q,a) <- delta.keys.toSeq) yield (q,Some(a))
  def _delta(q: State, x: Option[Letter]) = x match {
    case Some(a) if delta.contains((q,a)) => Set(delta((q,a)))
    case _ => Set.empty
  }
  def simplify: DFA[State,Letter] = {
    val Q1 = reachableStates(Set(q0))
    val delta1 = delta.filterKeys(qa => Q1.contains(qa._1))
    DFA(Q1, Sigma, delta1, q0, F.intersect(Q1))
  }
}

case class NFA[State,Letter](
  Q: Set[State],
  Sigma: Set[Letter],
  delta: Map[(State,Letter),Set[State]],
  q0: State,
  F: Set[State]
) extends Automaton(Q, Sigma, q0, F) {
  def _deltaKeys = for ((q,a) <- delta.keys.toSeq) yield (q,Some(a))
  def _delta(q: State, x: Option[Letter]) = x match {
    case Some(a) if delta.contains((q,a)) => delta((q,a))
    case _ => Set.empty
  }
  def simplify: NFA[State,Letter] = {
    val Q1 = reachableStates(Set(q0))
    val delta1 = delta.filterKeys(qa => Q1.contains(qa._1))
    NFA(Q1, Sigma, delta1, q0, F.intersect(Q1))
  }
}

case class eNFA[State,Letter](
  Q: Set[State],
  Sigma: Set[Letter],
  delta: Map[(State,Option[Letter]),Set[State]],
  q0: State,
  F: Set[State]
) extends Automaton(Q, Sigma, q0, F) {
  def _deltaKeys = delta.keys.toSeq
  def _delta(q: State, x: Option[Letter]) =
    delta.getOrElse((q,x), Set.empty)
  def simplify: eNFA[State,Letter] = {
    val Q1 = reachableStates(Set(q0))
    val delta1 = delta.filterKeys(qa => Q1.contains(qa._1))
    eNFA(Q1, Sigma, delta1, q0, F.intersect(Q1))
  }
}
